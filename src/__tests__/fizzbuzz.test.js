import { fizzbuzz } from '../fizzbuzz'

describe('fizzbuzz', () => {
  it('fizzbuzz 3 to equal fizz', () => {
    expect(fizzbuzz(3)).toBe('fizz')
  })
  it('fizzbuzz 5 to qual buzz', () => {
    expect(fizzbuzz(5)).toBe('buzz')
  })
  it('fizzbuzz 15 to equal fizzbuzz', () => {
    expect(fizzbuzz(15)).toBe('fizzbuzz')
  })
  it('fizzbuzz input is not a number throw error', () => {
    expect(() => {
      fizzbuzz('a')
    }).toThrow()
  })
  it('fizzbuzz is not 3 5 15 will return input number', () => {
    expect(fizzbuzz(7)).toBe(7)
  })
})
